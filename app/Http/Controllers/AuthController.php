<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('register');
    }

    public function welcome(Request $request)
    {
        return view('welcome');
    }

    public function welcome_post(Request $request)
    {
        $data['first_name'] = strtoupper($request->input('first_name'));
        $data['last_name'] = strtoupper($request->input('last_name'));

        return view('welcome', ['data' => $data]);
    }
}
