<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Halaman | Buat Account Baru</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h3>Sign Up Form</h3>
	<form action="/welcome" method="POST">
		@csrf
		<label>First Name :</label> 
		<br><br>
		<input type="text" name="first_name"> 
		<br><br>
		<label>Last Name :</label> <br><br>
		<input type="text" name="last_name"> 
		<br><br>
		<label>Gender :</label> 
		<br><br>
		<input type="radio" name="gender" value="male">Male
		<br>
		<input type="radio" name="gender" value="female">Female 
		<br>
		<input type="radio" name="gender" value="other">Other 
		<br><br>
		<label>Nationality :</label>
		<br>
		<br>
		<select name="nationality">
			<option value="laki-laki">Indonesian</option>
			<option value="perempuan">Other</option>
		</select>
		<br><br>
		<label>Language Spoken :</label>
		<br><br>
		<input type="checkbox">Bahasa Indonesia<br>
		<input type="checkbox">English<br>
		<input type="checkbox">Other
		<br><br>
		<label>Bio :</label>
		<br><br>
		<textarea name="alamat" cols="30" rows="10"></textarea>
		<br><br>
		<input type="submit">
	</form>
</body>
</html>